#![allow(non_snake_case)]

use vtable::*;
use std::{os::raw::c_char, ffi::{c_void, CString, CStr}, ptr::null, mem::forget};

// VRef<IServerCallbacksVTable>

#[vtable]
#[repr(C)]
struct IServerPluginCallbacksVTable {
	Load: fn(VRef<IServerPluginCallbacksVTable>, *const (), *const ()) -> bool,
	Unload: fn(VRef<IServerPluginCallbacksVTable>),
	Pause: fn(VRef<IServerPluginCallbacksVTable>),
	UnPause: fn(VRef<IServerPluginCallbacksVTable>),
	GetPluginDescription: fn(VRef<IServerPluginCallbacksVTable>) -> *const c_char,
	LevelInit: fn(VRef<IServerPluginCallbacksVTable>, *const c_char),
	ServerActivate: fn(VRef<IServerPluginCallbacksVTable>, *const c_void, i32, i32),
	GameFrame: fn(VRef<IServerPluginCallbacksVTable>, bool),
	LevelShutdown: fn(VRef<IServerPluginCallbacksVTable>),
	ClientFullyConnect: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
	ClientActive: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
	ClientDisconnect: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
	ClientPutInServer: fn(VRef<IServerPluginCallbacksVTable>, *const c_void, *const c_char),
	SetCommandClient: fn(VRef<IServerPluginCallbacksVTable>, i32),
	ClientSettingsChanged: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
	ClientConnect: fn(VRef<IServerPluginCallbacksVTable>, *mut bool, *const c_void, *const c_char, *const c_char, *mut c_char, i32) -> i32,
	ClientCommand: fn(VRef<IServerPluginCallbacksVTable>, *const c_void, *const c_void) -> i32,
	NetworkIDValidated: fn(VRef<IServerPluginCallbacksVTable>, *const c_char, *const c_char) -> i32,
	OnQueryCvarValueFinished: fn(VRef<IServerPluginCallbacksVTable>, i32, *const c_void, i32, *const c_char, *const c_char),
	OnEdictAllocated: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
	OnEdictFreed: fn(VRef<IServerPluginCallbacksVTable>, *const c_void),
}

struct Plugin();

impl IServerPluginCallbacks for Plugin {
	fn Load(&self, _interfaceFactory: *const (), _gameServerFactory: *const ()) -> bool {
		true
	}
	fn Unload(&self) { }
	fn Pause(&self) { }
	fn UnPause(&self) { }
	fn GetPluginDescription(&self) -> *const c_char {
		let s = CString::new("Rust Plugin").unwrap();
		let p = s.as_ptr();
		forget(p);
		p
	}
	fn LevelInit(&self, _pMapName: *const c_char) { }
	fn ServerActivate(&self, _pEdictList: *const c_void, _edictCount: i32, _clientMax: i32) { }
	fn GameFrame(&self, _simulating: bool) { }
	fn LevelShutdown(&self) { }
	fn ClientFullyConnect(&self, _pEdict: *const c_void) { }
	fn ClientActive(&self, _pEntity: *const c_void) { }
	fn ClientDisconnect(&self, _pEntity: *const c_void) { }
	fn ClientPutInServer(&self, _pEntity: *const c_void, _playername: *const c_char) { }
	fn SetCommandClient(&self, _index: i32) { }
	fn ClientSettingsChanged(&self, _pEdict: *const c_void) { }
	fn ClientConnect(&self, _bAllowConnect: *mut bool, _pEntity: *const c_void, _pszName: *const c_char, _pszAddress: *const c_char, _reject: *mut c_char, _maxrejectlen: i32) -> i32 { 0 }
	fn ClientCommand(&self, _pEntity: *const c_void, _args: *const c_void) -> i32 { 0 }
	fn NetworkIDValidated(&self, _pszUserName: *const c_char, _pszNetworkID: *const c_char) -> i32 { 0 }
	fn OnQueryCvarValueFinished(&self, _iCookie: i32, _pPlayerEntity: *const c_void, _eStatus: i32, _pCvarName: *const c_char, _pCvarValue: *const c_char) { }
	fn OnEdictAllocated(&self, _edict: *const c_void) { }
	fn OnEdictFreed(&self, _edict: *const c_void) { }
}

IServerPluginCallbacksVTable_static!(static PLUGIN_VT for Plugin);

static plugin: Plugin = Plugin { };

#[allow(unused_variables)]
fn CreateInterfaceInternal(pName: *const c_char, pReturnCode: *mut i32) -> *const c_void {
	let s = unsafe { CStr::from_ptr(pName) };
	if s.to_str().unwrap() == "ISERVERPLUGINCALLBACKS002" {
		// TODO: need to return "address of" plugin as *const c_void
	}
	unsafe { pReturnCode.as_mut().map(|x| *x = 1) };
	null()
}

#[no_mangle]
pub extern fn CreateInterface(pName: *const c_char, pReturnCode: *mut i32) -> *const c_void {
	CreateInterfaceInternal(pName, pReturnCode)
}
